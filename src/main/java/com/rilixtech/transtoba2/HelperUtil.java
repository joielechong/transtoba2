package com.rilixtech.transtoba2;

public class HelperUtil {

    public static String hex2asc(String in) {
        String temp = in;
        String out = "";
        int pi = 0;

        while (temp.length() != 0) {
            pi = Integer.parseInt(temp.substring(0, 4), 16);
            out = out + (char) pi;
            temp = temp.substring(4);
        }
        return out;
    }

    /******************************************************************************
     little helpers, probably not used currently
     ******************************************************************************/
    public static boolean tobaIsKonsonant(char in) {
        char[] kons = {0x61, 0x68, 0x6B, 0x62, 0x70, 0x6E, 0x77, 0x67, 0x6A, 0x64, 0x72, 0x6D, 0x74, 0x73, 0x79, 0x3C, 0x6C, 0x00};
        for (int x = 0; in != kons[x]; x++) {
            if (kons[x] == 0x00) return false;
        }
        return true;
    }

    public static boolean tobaIsKonsonantU(char in) {
        char[] k_u = {0x41, 0x48, 0x4B, 0x42, 0x50, 0x4E, 0x57, 0x47, 0x4A, 0x44, 0x52, 0x4D, 0x54, 0x53, 0x59, 0x3E, 0x4C, 0x00}; // ,0x5D removed for new ny(u) rule, version 0.9
        for (int x = 0; in != k_u[x]; x++) {
            if (k_u[x] == 0x00) return false;
        }
        return true;
    }

    public static boolean romanIsVokal(char in) {
        char[] dv = {'A', 'E', 'I', 'O', 'U', 0x00};
        for (int x = 0; dv[x] != 0x00; x++) {
            if (dv[x] == in) return true;
        }
        return false;
    }

    public static boolean tobaIsDiacritic(char in) {
        char[] dia = {0x5C, 0x65, 0x69, 0x6F, 0x78, 0x00};
        for (int x = 0; in != dia[x]; x++) {
            if (dia[x] == 0x00) return false;
        }
        return true;
    }
}
